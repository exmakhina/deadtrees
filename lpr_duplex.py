#!/usr/bin/env python
# -*- coding: utf-8 vi:noet
# Perform manual duplex printing of a sequence of PDFs

import re
import argparse
import logging
import subprocess
import tempfile
import os
import shutil
from contextlib import contextmanager

from pypdf import PdfReader, PdfWriter


logger = logging.getLogger()


def setup_parser():
	parser = argparse.ArgumentParser(
	 description="Duplex printing tool",
	 add_help=False,
	)
	parser.add_argument(
	 "--log-level",
	 default="INFO",
	 help="Logging level (eg. INFO, see Python logging docs)",
	)

	parser.add_argument(
	 "--keep-files",
	 type=int,
	 choices=(0, 1),
	 default=0,
	 help="Keep (1) or delete (0) temporary files after printing",
	)

	parser.add_argument(
	 "--doit",
	 type=int,
	 choices=(0, 1),
	 default=0,
	 help="Don't pretend",
	)

	for o in "hlmpqrE":
		parser.add_argument(f"-{o}",
		 action='store_true',
		 help='LPR option that does not require an argument',
		)

	for o in "CJTPU#":
		parser.add_argument(f"-{o}",
		 help='LPR option that requires an argument',
		 nargs=1,
		)

	for o in "o":
		parser.add_argument(f"-{o}",
		 help='LPR option that requires an argument (append)',
		 action="append",
		 nargs=1,
		)

	parser.add_argument(
	 "files",
	 nargs='+',
	 help="Files to print"
	)

	return parser


@contextmanager
def get_tempdir(keep: int):
	d = tempfile.mkdtemp()
	yield d
	if keep:
		logger.info("Files are saved at %s", d)
		return
	shutil.rmtree(d)


def main():
	parser = setup_parser()
	args = parser.parse_args()

	logging.basicConfig(
	 datefmt="%Y%m%dT%H%M%S",
	 level=getattr(logging, args.log_level.upper(), logging.INFO),
	 format="%(asctime)-15s %(name)s %(levelname)s %(message)s"
	)

	lpr_options = []
	default = object()
	for action in parser._actions:
		if action.help.startswith("LPR option"):
			if (v := getattr(args, action.dest)) is not action.default:
				if action.nargs == 0:
					lpr_options += action.option_strings
				elif isinstance(action, argparse._AppendAction):
					for a in v:
						lpr_options += action.option_strings + a
				else:
					lpr_options += action.option_strings + v

	logger.info("LPR options: %s", lpr_options)
	logger.info("Files: %s", args.files)

	pages = dict()

	for path in args.files:
		if not path.lower().endswith(".pdf"):
			raise ValueError(f"Not a PDF: {path}")
		if not os.path.isfile(path):
			raise ValueError(f"Missing {path}")

		reader = PdfReader(path)
		pages[path] = len(reader.pages)

	logger.info("Pages: %s", pages)


	with get_tempdir(args.keep_files) as tempdir:
		odd_path = os.path.join(tempdir, "odd.pdf")
		reversed_even_path = os.path.join(tempdir, "even.pdf")

		odd = PdfWriter()
		for file in args.files:
			odd.append(file, pages=[x for x in range(pages[file]) if (x+1) % 2 == 1])
		odd.write(odd_path)

		reversed_even = PdfWriter()
		logger.info("%s", args.files)
		for file in reversed(args.files):
			if pages[file] % 2 == 1:
				reversed_even.add_blank_page(1,1)
			reversed_even.append(file, pages=list(
			 reversed([x for x in range(pages[file]) if (x+1) % 2 == 0])
			))
		reversed_even.write(reversed_even_path)

		logging.info("Printing odd pages")

		if args.doit:
			subprocess.run(["lpr"] + lpr_options + [odd_path], check=True)

		input("Press Enter after reloading paper for even pages...")

		logging.info("Printing reversed even pages")
		if args.doit:
			subprocess.run(["lpr"] + lpr_options + [reversed_even_path], check=True)


if __name__ == '__main__':
	main()
